FROM nodered/node-red-docker:v8

VOLUME ["/data"]

USER root

RUN echo "deb http://ftp.debian.org/debian jessie-backports main" | tee /etc/apt/sources.list.d/jessie-backports.list \
	&& apt update \
	&& apt install -t jessie-backports -y ca-certificates-java openjdk-8-jre-headless

RUN npm install git+https://iroha.miaounyan.eu/piernov/node-red-contrib-thingml-compiler

#USER node-red
