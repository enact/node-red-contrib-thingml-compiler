//This function call ThingML for compilation
var spawn = require('child_process').spawn;
var fs = require('fs');
var path = require('path');
var events = require('events');
var AdmZip = require('adm-zip');

function cleanDirectory(dirpath) {
    if (!fs.existsSync(dirpath)) {
        return;
    }
    if (!fs.lstatSync(dirpath).isDirectory()) {
        fs.unlinkSync(dirpath);
        return;
    }

    var files = fs.readdirSync(dirpath);
    if (files) {
        files.forEach(function(file) {
            var filepath = path.resolve(dirpath, file);
            if (fs.lstatSync(filepath).isDirectory()) {
                cleanDirectory(filepath);
                fs.rmdirSync(filepath);
            } else {
                fs.unlinkSync(filepath);
            }
        });
    }
}

module.exports = class ThingMLCompilerEngine {
    constructor() {
        this.emitter = new events.EventEmitter();
    }

    unzipSources(source, callback) {
        try {
            var zip = new AdmZip(source);
            zip.extractAllTo('./', true);
        } catch (e) { // zip file missing or corrupted
            this.emitter.emit('error', e);
            return;
        }
        callback();
    }

    spawnCompiler(node, config, source) {
        var engine = this;
        var hasError = false;

        node.process = spawn('java', [
            '-jar', __dirname + '/' + '../lib/thingml/ThingML2CLI.jar',
            '-c', config.target,
            '-s', source,
            '-o', config.output
        ]);

        node.process.stdout.setEncoding('utf8');
        node.process.stdout.on('data', (data) => {
            data.trim().split('\n').forEach(line => {
                if (line.toLowerCase().startsWith('warning')) {
                    node.warn(line);
                } else if (line.toLowerCase().startsWith('error')) {
                    hasError = true;
                    node.error(line);
                } else {
                    node.log(line);
                }
            });
        });
        node.process.stderr.setEncoding('utf-8');
        node.process.stderr.on('data', (data) => {
            data.trim().split('\n').forEach(line => {
                if (!line.toLowerCase().startsWith('warning')) {
                    hasError = true;
                    node.error(line);
                }else{
                    node.log(data);
                }
            });
        });

        node.process.on('error', (err) => {
            hasError = true;
            node.error('Something went wrong with the compiler! ' + err);
        });

        node.process.on('exit', (code) => {
            if (code !== 0) {
                hasError = true;
                node.log("Error code: "+code);
            }
            if (hasError) {
                node.error('Cannot complete because of errors!');
                engine.emitter.emit('error');
            } else {
                node.log('Done!');
                engine.emitter.emit('built');
            }
            delete node.process;
        });
    }

    build(node, config) {
        var engine = this;
        config.output = 'generated';

        var productsPath = config.output + '/' + config.name;

        cleanDirectory(productsPath);

        if (config.target != 'arduino') {
            config.output = productsPath
        }

        //TODO: what to do if file exists and code is typed in? For now it overwrites the file
        var source = config.source;
        if (source === '') {
            source = config.name + '.thingml';

            fs.writeFile(source, config.code, function (err) {
                if (err) {
                    return node.error(err);
                }
                node.log("The file was saved as " + source);
                engine.spawnCompiler(node, config, source);
            });

        } else if (config.sourcetype === 'application/zip') {
            engine.unzipSources(source, function() {
                node.log(source + ' extracted');
                engine.spawnCompiler(node, config, config.sourcepath);
            });
        } else if (config.sourcetype === 'application/gzip' || config.sourcetype === 'application/x-tar') {
            engine.untarSources(source, function() {
                node.log(source + ' extracted');
                engine.spawnCompiler(node, config, config.sourcepath);
            });
        } else {
            engine.spawnCompiler(node, config, source);
        }
    }

    close(node) {
        if (node.process) {
            node.process.kill();
        }
        delete node.process;
        this.emitter.removeAllListeners();
    };

    on(e, f) {
        this.emitter.on(e, f);
    }
}
