var fs = require('fs');
var multer  = require('multer');

function build(engine, node, config) {
    node.status({fill:"yellow",shape:"dot",text:"thingml-compiler.status.building"});
    engine.build(node, config);
}

module.exports = function(RED) {
    "use strict";
    var ThingMLCompilerEngine = require("./thingml-engine");

    function ThingMLCompilerNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        var engine = new ThingMLCompilerEngine();

        engine.on('built', function() {
            node.status({fill:"green",shape:"dot",text:"thingml-compiler.status.success"});
            var msg = { payload: config };
            node.send(msg);
        });

        engine.on('error', function(e) {
            node.status({fill:"red",shape:"ring",text:"thingml-compiler.status.error"});
            node.error(e);
        });

        // Build on instanciation
        if (config.active) {
            build(engine, node, config);
        }

        // Received message, trigger build
        //TODO: if we trigger build by message and the previous build didn't finish, what to do?
        node.on('input', function(msg) {
            build(engine, node, config);
        });

        // cleanup on flow stop
        node.on("close", function (done) {
            engine.close(node);
            done();
        });
    }
    RED.nodes.registerType("thingml-compiler", ThingMLCompilerNode);

    RED.httpAdmin.post("/thingml-compiler/:id/active/:state", RED.auth.needsPermission("thingml-compiler.write"), function(req,res) {
        var node = RED.nodes.getNode(req.params.id);
        var state = req.params.state;
        if (node !== null && typeof node !== "undefined" ) {
            if (state === "enable") {
                node.active = true;
                res.sendStatus(200);
                if (node.tostatus) { node.status({}); }
            } else if (state === "disable") {
                node.active = false;
                res.sendStatus(201);
                if (node.tostatus && node.hasOwnProperty("oldStatus")) {
                    node.oldStatus.shape = "ring";
                    node.status(node.oldStatus);
                }
            } else {
                res.sendStatus(404);
            }
        } else {
            res.sendStatus(404);
        }
    });

    RED.httpAdmin.post("/thingml-compiler/upload", RED.auth.needsPermission("thingml-compiler.write"), function(req,res) {
        var storage = multer.diskStorage({
            destination: "./",
            filename: function (req, file, cb) {
                cb(null, file.originalname)
            }
        });
        var upload = multer({ storage: storage }).any();
        upload(req, res, function(err) {
            if (err) {
                console.log(err);
                res.sendStatus(500);
            } else {
                res.sendStatus(200);
            }
        });
    });
}
